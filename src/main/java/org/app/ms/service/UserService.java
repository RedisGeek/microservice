package org.app.ms.service;

import org.app.ms.dao.UserRepository;
import org.app.ms.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")

public class UserService {

    @Autowired
    private UserRepository userRepository;

    @GetMapping(value = "/users")
    public List<User> getUser(){
        return userRepository.findAll();
    }

    @GetMapping(value = "/user/{id}")
    public User listUser(@PathVariable(name = "id") String id){
        return userRepository.findById(id).get();
    }

    @PutMapping(value="/user/edit/{id}")
    public User updateUser(@PathVariable(name = "id") String id , @RequestBody User us){
        us.setId(id);
        return userRepository.save(us);
    }

    @PostMapping(value = "/user/register")
    public User saveUser(@RequestBody User us){
        return userRepository.save(us);
    }

    @DeleteMapping(value = "/user/delete/{id}")
    public void deleteUser(@PathVariable(name = "id") String id){
        userRepository.deleteById(id);
    }

}
