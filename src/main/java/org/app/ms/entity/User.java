package org.app.ms.entity;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.Set;

@Document(collection = "user")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter

public class User {

    @Id
    private String id;

    @Indexed(unique = true , direction = IndexDirection.DESCENDING, dropDups = true)
    private String nom;
    private String prenom;
    private Date date_naissance;
    private String status;
    private String email;
    private String mot_de_passe;

    @DBRef
    private Set<Role> roles;

    public void setId(String id) {
    }
}
